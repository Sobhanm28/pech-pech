import 'package:get_it/get_it.dart';

import 'package:pech_pech/feature/landing/presentation/bloc/bloc_landing.dart';

final sl = GetIt.instance;

void init() {
  //Features
  sl.registerFactory(() => BlocLanding());
}
