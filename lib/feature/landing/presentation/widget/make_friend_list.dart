import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

AnimationLimiter makeFriendList(BuildContext context, List list) {
  return AnimationLimiter(
    child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredGrid(
            position: index,
            duration: Duration(milliseconds: 600),
            columnCount: list.length,
            child: SlideAnimation(
              horizontalOffset: -50,
              child: FadeInAnimation(
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        color: Colors.grey.shade300),
                    margin: EdgeInsets.symmetric(vertical: 10),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Text(
                      list[index]["message"],
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
  );
}
