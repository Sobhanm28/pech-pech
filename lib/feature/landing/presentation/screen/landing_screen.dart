import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pech_pech/feature/landing/presentation/bloc/bloc_landing.dart';
import 'package:pech_pech/feature/landing/presentation/bloc/event_landing.dart';
import 'package:pech_pech/feature/landing/presentation/bloc/state_landing.dart';
import 'package:pech_pech/feature/landing/presentation/widget/make_friend_list.dart';
import 'package:pech_pech/feature/message/presentation/screen/message_screen.dart';

import '../../../../injection_container.dart';

class LandingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        label: Text("New Message"),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MessageScreen(
                        friendName: "Sahand",
                      )));
        },
      ),
      appBar: AppBar(
        title: Text("Last Messages"),
      ),
      body: BlocProvider(
        builder: (_) => sl<BlocLanding>(),
        child:
            BlocBuilder<BlocLanding, StateLanding>(builder: (context, state) {
          if (state is Loading) {
            Future.delayed(Duration(seconds: 1), () {
              BlocProvider.of<BlocLanding>(context).add(OnLoaded());
            });
            return Center(child: CircularProgressIndicator());
          } else if (state is Loaded) {
            return Container(
              alignment: Alignment.center,
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection("Sahand")
                      .orderBy("time", descending: true)
                      .limit(5)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return CircularProgressIndicator();
                    }
                    return makeFriendList(context, snapshot.data.documents);
                  }),
            );
          } else if (state is Error) {
            return Center(child: Text("Error"));
          }
        }),
      ),
    );
  }
}
