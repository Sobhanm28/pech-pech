import 'package:equatable/equatable.dart';

abstract class StateLanding extends Equatable {}

//Runs for the first time in the [landing_screen]
class Loading extends StateLanding {}

//when app face with an exception [Error] is run
class Error extends StateLanding {
  final String message;

  Error({this.message});
}

//when data is ready for display to the user [Loaded] is run
class Loaded extends StateLanding {}
