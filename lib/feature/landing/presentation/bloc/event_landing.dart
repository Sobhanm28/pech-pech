import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class EventLanding extends Equatable {}

//call the [OnLoading] for Loading own dependency
class OnLoading extends EventLanding {}

//when own dependency is ready for display to user [OnLoaded] is running
class OnLoaded extends EventLanding {
  final AsyncSnapshot snapshot;

  OnLoaded({this.snapshot});
}
