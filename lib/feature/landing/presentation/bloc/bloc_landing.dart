import 'package:pech_pech/feature/landing/presentation/bloc/state_landing.dart';
import 'package:bloc/bloc.dart';
import 'event_landing.dart';

class BlocLanding extends Bloc<EventLanding, StateLanding> {
  @override
  StateLanding get initialState {
    return Loading();
  }

  @override
  Stream<StateLanding> mapEventToState(EventLanding event) async* {
    if (event is OnLoaded) {
      yield Loaded();
    }
  }
}
