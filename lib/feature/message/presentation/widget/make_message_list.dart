import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

ListView makeMessageList(BuildContext context, List listData) {
  const APP_ID = "Sobhan";
  return ListView.builder(
      itemCount: listData.length,
      itemBuilder: (context, index) {
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: Duration(milliseconds: 500),
          child: FadeInAnimation(
            child: Container(
              alignment: listData[index]["from"] == APP_ID
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Text(
                  listData[index]["message"],
                  style: TextStyle(
                      color: listData[index]["from"] == APP_ID
                          ? Colors.white
                          : Colors.blue,
                      fontSize: 20),
                ),
                decoration: BoxDecoration(
                    color: listData[index]["from"] == APP_ID
                        ? Colors.blue
                        : Colors.grey.shade300,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
          ),
        );
      });
}
