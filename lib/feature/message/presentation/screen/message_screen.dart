import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pech_pech/feature/message/presentation/widget/make_message_list.dart';

class MessageScreen extends StatefulWidget {
  String friendName;

  MessageScreen({Key key, this.friendName}) : super(key: key);

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  static const APP_ID = "Sobhan";
  TextEditingController msgController = TextEditingController();
  GlobalKey<AnimatedListState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Messages"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 9,
              child: StreamBuilder(
                stream: Firestore.instance
                    .collection(widget.friendName)
                    .orderBy("time")
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return makeMessageList(context, snapshot.data.documents);
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              flex: 1,
              child: TextField(
                controller: msgController,
                textAlign: TextAlign.end,
                decoration: InputDecoration(
                    suffixIcon: InkWell(
                        onTap: () {
                          Firestore.instance
                              .collection(widget.friendName)
                              .document()
                              .setData({
                            "from": APP_ID,
                            "to": widget.friendName,
                            "message": msgController.text,
                            "time": DateTime.now()
                          });
                          msgController.clear();
                        },
                        child: Icon(Icons.send)),
                    hintText: "پیام خود را وارد کنید",
                    border: InputBorder.none),
              ),
            )
          ],
        ),
      ),
    );
  }
}
